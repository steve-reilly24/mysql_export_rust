use mysql::PooledConn;
use std::error::Error;

pub fn conn() -> Result<PooledConn, Box<dyn Error>> {
    let pool = mysql::Pool::new("connection_string")?;
    Ok(pool.get_conn().unwrap())
}