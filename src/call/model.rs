use chrono::NaiveDateTime;
use std::error::Error;

#[derive(Debug, PartialEq, Eq, Serialize)]
pub struct Call {
    pub id: i32,
    pub direction: String,
    pub created_at: NaiveDateTime,
    pub updated_at: NaiveDateTime,
}


