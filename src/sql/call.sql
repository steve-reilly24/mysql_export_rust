SELECT id,
    direction, 
    created_at, 
    updated_at 
FROM db.call
where created_at >= DATE_SUB(NOW(),INTERVAL 30 DAY);