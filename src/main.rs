#[macro_use]
extern crate serde_derive;
mod db;
mod call;

use std::error::Error;
use std::process;
use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::path::Path;
use csv;


fn run() -> Result<(), Box<dyn Error>> {
    let filepath = Path::new("test.csv");
    
    // Open a file in write-only mode, returns `io::Result<File>`
    File::create(filepath)?;
    let mut wtr = csv::Writer::from_path(filepath)?;

    let mut conn = db::conn()?;

    let sqlpath = Path::new("src/sql/call.sql");
    let mut file = File::open(sqlpath)?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
 
    conn.query(contents)
        .unwrap()
        .for_each(|call| {
        
            let (id, 
                direction,
                updated_at,
                created_at) = mysql::from_row(call.unwrap());
                
            let c = call::Call {
                id: id,
                direction: direction,
                created_at: created_at,
                updated_at: updated_at,
            };
            wtr.serialize(c).unwrap();
        });

    wtr.flush()?;
        
    Ok(())
}


fn read_sql_file(path: &Path) -> Result<String, Box<dyn Error>> {
    let mut file = File::open(path)?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    Ok(contents)
}

fn main() {
    if let Err(err) = run() {
        println!("{}", err);
        process::exit(1);
    }
}